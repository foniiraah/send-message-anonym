import Vue from "vue";
import VueRouter from 'vue-router';
import Hasil  from "@/components/FormMessage.vue"
import Dashboard from "@/components/Dashboard.vue"

Vue.use(VueRouter);
export default new VueRouter({
  routes: [
    {
      path: '/',
      component: Dashboard,
      meta: {
        requiresAuth: true
      },
    },
    {
      path: '/form-message',
      component: Hasil,
      meta: {
        requiresAuth: true
      }
    }
  ]
});
