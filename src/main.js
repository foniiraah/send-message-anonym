import Vue from 'vue'
import App from './App.vue'
import Particles from "particles.vue";
import Vuetify from 'vuetify'
import vuetify from './plugins/vuetify'
import router from './router/index'

Vue.use(Particles);
Vue.use(Vuetify);
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
