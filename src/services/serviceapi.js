import axios from 'axios'


export class Services {
    GetLoadData(param) {
        const url = `https://localhost:1323/api/loadData`
        const data = axios
          .get(url, param)
          .then(response => response.data)
          .catch(err => err)
        return data 
    }

    
}